package com.bleconnection;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class Logs2File {

    public static SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy_HHmmss", Locale.getDefault());


    public void writeLogs() {
        try
        {

            String currentDateAndTime = sdf.format(new Date());

            File file = new File(Environment.getExternalStorageDirectory().
                    getAbsolutePath()+ Constants.STORAGE_FOLDER_PATH);
            if(!file.exists()){
                file.mkdir();
            }
            File logFile = new File(file+"/log_" + currentDateAndTime + ".txt");
            if(!Objects.requireNonNull(logFile.getParentFile()).exists()) {
                logFile.getParentFile().mkdirs();
            }

            if(!logFile.exists()) {
                try {
                    boolean fileCreated = logFile.createNewFile();
                    Log.e("@@@ writeLogs in Logs2File fileCreated: ",fileCreated+", path: "+logFile.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Runtime.getRuntime().exec("pm grant com.live.stream android.permission.READ_LOGS");

            String cmd = "logcat -f " + logFile.getAbsolutePath();
            Runtime.getRuntime().exec(cmd);

        } catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
