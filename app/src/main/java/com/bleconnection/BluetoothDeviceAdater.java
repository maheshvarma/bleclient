package com.bleconnection;

import android.net.wifi.WifiConfiguration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BluetoothDeviceAdater extends RecyclerView.Adapter<BluetoothDeviceAdater.DeviceViewHolder> {

    private final List<WifiConfiguration> wifiConfigurationList;

    public BluetoothDeviceAdater(List<WifiConfiguration> wifiConfigurationList) {
        this.wifiConfigurationList = wifiConfigurationList;
    }

    @NonNull
    @Override
    public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.bluetooth_item,parent,false);
        return new DeviceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceViewHolder holder, int position) {
        holder.mdeviceSSIDTV.setText(wifiConfigurationList.get(position).SSID);
    }

    @Override
    public int getItemCount() {
        return wifiConfigurationList.size();
    }

    static class DeviceViewHolder extends RecyclerView.ViewHolder{

        private final TextView mdeviceSSIDTV;

        public DeviceViewHolder(@NonNull View itemView) {
            super(itemView);
            mdeviceSSIDTV = itemView.findViewById(R.id.deviceSSIDTV);
        }
    }
}
