package com.bleconnection;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;

import java.util.UUID;

public class Utils {

    public static BluetoothGattCharacteristic provideCharacteristic(UUID characteristicUuid) {

        return new  BluetoothGattCharacteristic(
                characteristicUuid,
                BluetoothGattCharacteristic.PROPERTY_WRITE,
                BluetoothGattCharacteristic.PERMISSION_WRITE);
    }

    public static BluetoothGattDescriptor provideDescriptor(UUID descriptorUuid) {
        return new  BluetoothGattDescriptor(
                descriptorUuid,
                BluetoothGattCharacteristic.PERMISSION_WRITE);
    }
}
