package com.bleconnection;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;


import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    private final int ANDROID_12_BLE_PERMISSIONS = 100;
    private Handler mHandler;
    private BluetoothAdapter mBluetoothAdapter;

    private static final int REQUEST_ENABLE_BT = 1;
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 10000;
    private boolean mScanning = false;
    private BluetoothManager bluetoothManager;

    ScanCallback mClientScanCallback = null;
    BluetoothLeScanner mBluetoothLeScanner = null;
    private GattClientCallback mGattClientCallback;
    private WifiManager wifiManager;
    private String ssid;
    private BluetoothGattCharacteristic messageCharacteristic;
    private EditText ssidEdText, pwdEdText;
    private Button submitBtn, mPlayBtn, mPauseBtn, mStopBtn;
    private String ssidStr, pwdStr;

    public static final int SECURITY_NONE = 0;
    public static final int SECURITY_WEP = 1;
    public static final int SECURITY_PSK = 2;
    public static final int SECURITY_EAP = 3;
    private MainActivity context;
    private LocationManager locationManager;
    private boolean isGpsProviderEnabled;
    private int PERMISSION_REQUEST_COARSE_LOCATION = 10;
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    private int MY_PERMISSIONS_REQUEST_CODE = 111;
    private ProgressDialog progressDialog;
    private boolean isBleDeviceFound = false;
    private BluetoothGatt bluetoothGatt;
    private String mCommand = "";

    public class BluetoothScanCallback extends ScanCallback {

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            Log.d("onScanResult", result.toString());
            if (result.getDevice() != null && !isBleDeviceFound) {
                isBleDeviceFound = true;
                addScannedDevice(result);
                stopScanning(false);
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            Log.d("onBatchScanResults", results.get(0).toString());
            for (ScanResult result : results) {
                if (result.getDevice() != null && !isBleDeviceFound) {
                    isBleDeviceFound = true;
                    addScannedDevice(result);
                    stopScanning(false);
                }
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.d("errorCode=", errorCode + "");
            stopScanning(false);
            mScanning = false;
        }
    }


    private class GattClientCallback extends BluetoothGattCallback {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            //super.onConnectionStateChange(gatt, status, newState);
            handleOnConnectionStateChange(gatt, status, newState);
            Log.d("Gatt:onConnectionStateChange", gatt.getDevice().toString());
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            handleOnServicesDiscovered(gatt, status);
            Log.d("Gatt:onServicesDiscovered", gatt.getDevice().toString());
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth not supported", Toast.LENGTH_SHORT).show();
        }

        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                showLogs();
            }
        }).start();
       /* IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(wifiScanReceiver, intentFilter);*/

        ssidEdText = findViewById(R.id.editTextSsid);
        pwdEdText = findViewById(R.id.editTextPwd);

        submitBtn = findViewById(R.id.submit_btn);

        mPlayBtn = findViewById(R.id.playbtn);
        mPauseBtn = findViewById(R.id.pausebtn);
        mStopBtn = findViewById(R.id.stopbtn);

        mPlayBtn.setOnClickListener(this);
        mPauseBtn.setOnClickListener(this);
        mStopBtn.setOnClickListener(this);

        submitBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ssidStr = ssidEdText.getText().toString().trim();
                pwdStr = pwdEdText.getText().toString().trim();
                if (!mScanning) {
                    if (ssidStr != null && !ssidStr.isEmpty() && pwdStr != null && !pwdStr.isEmpty()) {
                        startScanning(Constants.WIFI);
                    } else {
                        showToast(MainActivity.this, "SSID or Password is empty");
                    }
                } else {
                    showToast(MainActivity.this, "Scanning is in progress,Try again");
                }
            }
        });
    }


    private void startScanning(String command) {
        showToast(this, "scanning Started");
        mScanning = true;
        mCommand = command;
        ArrayList filters = new ArrayList<ScanFilter>();
        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .setReportDelay(0)
                .build();
        filters.add(new ScanFilter.Builder()
                .setServiceUuid(new ParcelUuid(UUID.fromString(Constants.SERVICE_ID)))
                .build());

        if (mClientScanCallback == null) {
            mClientScanCallback = new BluetoothScanCallback();// Will setup this in 3rd step
        }
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        if (mBluetoothLeScanner == null) {
        } else {
            mBluetoothLeScanner.startScan(filters, settings, mClientScanCallback);
            showProgressDialog("Scanning Started");
            isBleDeviceFound = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(!isBleDeviceFound){
                        stopScanning(true);
                    }
                }
            }, 60000);
        }

    }

    private void enableLocation() {

        if (builder == null) {
            builder = new AlertDialog.Builder(this);
            builder.setTitle("Location Permission");
            builder.setMessage("The app needs location permissions. Please grant this permission to continue using the features of the app.");
            builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            builder.setNegativeButton(android.R.string.no, null);
            alertDialog = builder.create();
            alertDialog.show();
        } else {
            if (!alertDialog.isShowing()) {
                alertDialog.show();
            }
        }

    }

    private void stopScanning(boolean showMsg) {
        mBluetoothLeScanner.stopScan(mClientScanCallback);
        mClientScanCallback = null;
        mScanning = false;
        dismissProgressDialog();

        if(showMsg)
        showToast(this, "scanning stopped");
    }


    private void addScannedDevice(ScanResult result) {

        Log.d("addScannedDevice", "result");
        BluetoothDevice device = result.getDevice();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                connectWithScannedDevice(device);
            }
        }, 1000);
    }


    private void connectWithScannedDevice(BluetoothDevice device) {
       /* GattClientCallback mGattClientCallback = (GattClientCallback)
                mGattClientCallbackMap.get(device);*/
        if (mGattClientCallback == null) {
            mGattClientCallback = new GattClientCallback();
        }
        bluetoothGatt = device.connectGatt(this, false,
                mGattClientCallback);

        updateBleProgressTitle("connecting with device");
    }


    @Override
    protected void onResume() {
        super.onResume();

        isGpsProviderEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int wifipermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE);
        int writeStorageCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        if (!mBluetoothAdapter.isEnabled()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.BLUETOOTH_SCAN,
                        Manifest.permission.BLUETOOTH_CONNECT}, ANDROID_12_BLE_PERMISSIONS);
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        } else if (!isGpsProviderEnabled) {
            enableLocation();
        } else {
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            } else if (wifipermissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_WIFI_STATE}, 2);
            } else if (writeStorageCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);
            } else {
                //TODO
            }
        }

       /* // Initializes list view adapter.
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        mListView.setAdapter(mLeDeviceListAdapter);
        scanLeDevice(true);*/
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            Toast.makeText(this, "Bluetooth not Enabled", Toast.LENGTH_SHORT).show();
            return;
        } else if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_OK) {
            isGpsProviderEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            if (!isGpsProviderEnabled) {
                enableLocation();
            }
        } else if (requestCode == PERMISSION_REQUEST_COARSE_LOCATION) {

        } else if (requestCode == 3) {
            showLogs();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showLogs() {
        int writeStorageCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_WIFI_STATE);

        if (BuildConfig.DEBUG && writeStorageCheck == PackageManager.PERMISSION_GRANTED)
            new Logs2File().writeLogs();
    }

    private void handleOnConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        Log.d("OnConnectionStateChange", status + "");
        if (status == BluetoothGatt.GATT_FAILURE) {
            disconnectFromGattServer(gatt, false);
            return;
        } else if (status != BluetoothGatt.GATT_SUCCESS) {
            disconnectFromGattServer(gatt, true);
            return;
        }
        if (newState == BluetoothProfile.STATE_CONNECTED) {
            handleConnectedStateOfClient(gatt);
        } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            disconnectFromGattServer(gatt, true);
        }
    }

    private void handleConnectedStateOfClient(BluetoothGatt bluetoothGatt) {
        Log.d("discoverServices", "discoverServices");
        bluetoothGatt.discoverServices();
    }


    private void disconnectFromGattServer(BluetoothGatt gatt, Boolean retry) {
        if (retry) {
            gatt.connect();
        } else {
            gatt.disconnect();
            gatt.close();
        }
    }

    private void handleOnServicesDiscovered(BluetoothGatt gatt, int status) {
        Log.d("Gatt:ServicesDiscovered", "writeFirstDescriptor");
        if (gatt != null) {
            if (mCommand.length() > 0) {
                handleWrite(gatt, mCommand);
            }
        }

    }

    private void handleWrite(BluetoothGatt gatt, String command) {

        switch (command) {
            case "WIFI":
                String valuedata = ssidStr.trim() + "/" + pwdStr.trim();
                writeDescriptor(gatt, valuedata);
                break;
            case "PLAY":
                writeDescriptor(gatt, command);
            case "PAUSE":
                writeDescriptor(gatt, command);
            case "STOP":
                writeDescriptor(gatt, command);
                break;

        }
    }


    private void writeDescriptor(BluetoothGatt gatt, String message) {
        //showToast(MainActivity.this, "writeFirstDescriptor success");
        updateBleProgressTitle("Sending Data");
        BluetoothGattService service = gatt.getService(UUID.fromString(Constants.SERVICE_ID));

        if(service!=null){
            messageCharacteristic =
                    service.getCharacteristic(UUID.fromString(Constants.USER_CHARACTERIC_ID));
            messageCharacteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
            // String valuedata = ssidStr.trim() + "/" + pwdStr.trim();
            messageCharacteristic.setValue(message.getBytes(StandardCharsets.UTF_8));
            boolean status = gatt.writeCharacteristic(messageCharacteristic);
            if (status) {
                showToast(this, "Message sent successfully");
            } else {
                showToast(this, "Error sending message");
            }
        }else{
            showToast(this, "Try again");
        }
        stopScanning(true);

    }


    private void handleFailureOfDescriptionWrite(String address) {
        //Do nothing
    }


    private void handleFailureOfSendingMessage() {
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.playbtn:
                if (bluetoothGatt != null) {
                    writeDescriptor(bluetoothGatt, Constants.PLAY);
                } else {
                    startScanning(Constants.PLAY);
                }
                break;
            case R.id.pausebtn:
                if (bluetoothGatt != null) {
                    writeDescriptor(bluetoothGatt, Constants.PAUSE);
                } else {
                    startScanning(Constants.PAUSE);
                }
                break;
            case R.id.stopbtn:
                if (bluetoothGatt != null) {
                    writeDescriptor(bluetoothGatt, Constants.STOP);
                } else {
                    startScanning(Constants.STOP);
                }
                break;
        }
    }


    private void connectClientWifi(String ssid, String password) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
        if (list.size() > 0) {
            for (WifiConfiguration i : list) {
                if (i.SSID != null && i.SSID.equals("\"" + ssid + "\"")) {
                    String securityType = getSecurityType(i);
                    WifiConfiguration wifiConfiguration = createWifiConfiguration(ssid, password, securityType);
                    int res = wifiManager.addNetwork(wifiConfiguration);
                    Log.d("TAG", "# addNetwork returned " + res);

                    boolean b = wifiManager.enableNetwork(res, true);
                    Log.d("TAG", "# enableNetwork returned " + b);

                    wifiManager.setWifiEnabled(true);
                    break;
                }
            }
        } else {
            WifiConfiguration wifiConfig = new WifiConfiguration();

            wifiConfig.SSID = String.format("\"%s\"", ssid);
            wifiConfig.preSharedKey = String.format("\"%s\"", password);

            WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
            int netId = wifiManager.addNetwork(wifiConfig);
            wifiManager.disconnect();
            wifiManager.enableNetwork(netId, true);
            wifiManager.reconnect();
        }

    }

    private List<WifiConfiguration> getWifiList() {

        List<WifiConfiguration> list = new ArrayList<WifiConfiguration>();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            list = wifiManager.getConfiguredNetworks();

        }
        return list;
    }

    public static int getSecurity(WifiConfiguration config) {
        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_PSK))
            return SECURITY_PSK;

        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_EAP) || config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.IEEE8021X))
            return SECURITY_EAP;

        return (config.wepKeys[0] != null) ? SECURITY_WEP : SECURITY_NONE;
    }


    public static String getSecurityType(WifiConfiguration config) {
        switch (getSecurity(config)) {
            case SECURITY_WEP:
                return "WEP";
            case SECURITY_PSK:
                if (config.allowedProtocols.get(WifiConfiguration.Protocol.RSN))
                    return "WPA2";
                else
                    return "WPA";
            default:
                return "NONE";
        }
    }

    private WifiConfiguration createWifiConfiguration(String networkSSID, String networkPasskey, String securityMode) {
        WifiConfiguration wifiConfiguration = new WifiConfiguration();

        wifiConfiguration.SSID = "\"" + networkSSID + "\"";

        if (securityMode.equalsIgnoreCase("OPEN")) {

            wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

        } else if (securityMode.equalsIgnoreCase("WEP")) {

            wifiConfiguration.wepKeys[0] = "\"" + networkPasskey + "\"";
            wifiConfiguration.wepTxKeyIndex = 0;
            wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);

        } else if (securityMode.equalsIgnoreCase("PSK")) {

            wifiConfiguration.preSharedKey = "\"" + networkPasskey + "\"";
            wifiConfiguration.hiddenSSID = true;
            wifiConfiguration.status = WifiConfiguration.Status.ENABLED;
            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

        } else {
            return null;
        }

        return wifiConfiguration;

    }

    private void showToast(Context context, String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void showProgressDialog(String title) {

        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
        }
        progressDialog.setMessage(title);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void updateBleProgressTitle(String title) {
        if (progressDialog != null) {
            progressDialog.setMessage(title);
        }
    }

    private void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}